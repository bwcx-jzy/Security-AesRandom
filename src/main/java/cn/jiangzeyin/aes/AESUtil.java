package cn.jiangzeyin.aes;

/**
 * 关于加密解密的封装util方法类
 *
 * @author ChenRan
 * create 2017-10-20 16:47
 **/
public class AESUtil {


    /**
     * 解密
     *
     * @param data 密文
     * @return 解密后
     */
    public static String decode(String data) {
        return AES_Proxy.decode(data);
    }

    /**
     * 加密
     *
     * @param data 要加密的字符串
     * @return 加密后的
     */
    public static String encode(String data) {
        return AES_Proxy.encode(data);
    }
}
